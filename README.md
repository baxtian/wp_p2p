# DESCRIPTION

Create relationships between posts.

__Note__: this is a merak component and requires a Merak enviroment.

## Usage:

Create file to declare the relationships in __src/Relationships.php__

```
<?php

namespace App;

use Baxtian\WP_P2P as WP_P2P;

class Relationships extends WP_P2P
{
	use \Baxtian\SingletonTrait;

	/**
	 * Inicializa el componente
	 */
	protected function __construct()
	{
		parent::__construct(INV_F);
	}

	/**
	 * Declare relationships.
	 */
	public function connections(): void
	{

		$this->register_relationship([
			'name' 		=> 'post_to_page',
			'from' 		=> 'post',
			'to' 		=> 'page'
		]);

		$this->register_relationship([
			'name'	=> 'post_to_custom_post_type',
			'from'	=> [
				'type' 						=> 'post', 
				'title' 					=> __('Post', INV_D), 
				'cardinality' 				=> 'ONE', 
				'ui' 						=> 'auto',
				'icon'						=> 'admin-links' 
				'display_in_admin_column' 	=> 2,
				'allow_create'				=> true
			],
			'to'	=> [
				'type' 			=> 'custom_post_type', 
				'title' 		=> __('Custom post type', INV_D), 
				'cardinality' 	=> 'MANY', 
				'ui' 			=> 'manual'
			],
			'sortable' => false,
		]);
	}
}
```

Any new register requires an array with 3 or 4 arguments.

1. name: the name of the relationship
2. from: one of the structures to relate (post, page or custom types)
3. to: the other structure to relate
4. sortable: a boolean value to determine if the list allows to be sorted. This argument is not required and the default value is __true__.

Variables __from__ and __to__ could be the _slug name_ of the structure or an array with these arguments:

1. type: the _slug name_ of the structure (post, page, custom_post)
2. title: the text to use as title in the metabox. This argument is not required and the system will use by default the declared name of the structure in singular or plural depending on cardinality.
3. cardinality: if this is going to be a selection of one or many items. Allowed values are __ONE__ or __MANY__. This argument is not required and the default value is __MANY__.
4. ui: use __auto__ if this library is going to render the list on the settings sidebar of the structure, __sidebar__ to create a custom sidebar for itself or __manual__ to create the custom metabox. This argument is not required and the default value is __auto__.
5. icon: sets the [dashicon](https://developer.wordpress.org/resource/dashicons/) that will accompany the administration interface. This argument is not required and the default icon is __admin-links__.
6. display_in_admin_column: indicates whether items of this post type will be displayed in a column on the administration page. The number can be positive or negative to indicate the position in the columns of the table of the opposite post type. The defualt value is __false__.
7. allow_create: allows to create entry if no response while searching in the UI. The selector will display a pin near the text and will create the entry.

### Query

WP_P2P adds the ability to query the items related to a post using the __p2p__ argument.

This requieres an array that describes the type of relation and one of the final ends of the relationship.

In this example, the query returns any page that is related to post 5 and is related by the __post_to_page__ relationship. As you can see WP_P2P allows using __Timber::get_posts()__.

```
$args = [
	'post_type' => 'page',
	'p2p'       => [
		'relation' => 'AND',
		[
			'type' => 'post_to_page',
			'to'   => 5,
		],
	]
];

$posts = (array) Timber::get_posts($args);
```

In this example, the query returns any structure related by the __post_to_page__ relationship to posts 5 or 6. In this case the example uses the Wordpress __get_posts__ function.

```
$args = [
	'post_type' => 'any',
	'p2p'       => [
		'relation' => 'OR',
		[
			'type' => 'post_to_page',
			'to'   => 5,
		],
		[
			'type' => 'post_to_page',
			'to'   => 6,
		],
	]
];

$posts = get_posts($args);
```

Same reult as previws ecample but in this case the example uses the Wordpress __WP_Query__ class.

```
$args = [
	'post_type' => 'any',
	'p2p'       => [
		'relation' => 'OR',
		[
			'type' => 'post_to_page',
			'to'   => 5,
		],
		[
			'type' => 'post_to_page',
			'to'   => 6,
		],
	]
];

$query = new WP_Query($args);
```

In this example the query returns any structure related to the post number 5 with relationship __post_to_page__. In this Case the code uses a function directly form p2p.


```
use Baxtian\WP_P2P;

$list = WP_P2P::relationships('post_to_page', 5);
```

__Warning__: keep in mind this returns all related posts, even drafts, future and private posts. To use the filter capabilities of Wordpress it is suggested to use WP_Query, get_posts or Timber::get_posts().

### Render in twig / timber

To have a list of the related elements you can use:

```
{% set list = post|apply_filters('p2p/relationships', 'relationship_name') %}
```

This create a __list__ with an array of __WP_Posts__ related to __post__ by __relationship_name__. See the declaration to get the name of the relationship.

__Note:__ __post__ has to be of type __WP_Post__ or integer.

### Render custom metabox

In case the __ui__ option is __manual__ you must create your custom _metabox_. This has its own advantages, such as the ability to create a single _metabox_ for all relationships instead of having multiple _metaboxes_, one for each.

In this case you can use the plugin's UI to facilitate the creation of the _metabox_.

```
{% do action('p2p/metabox', post_id, 'relationship_name') %}
```

Where __post_id__ is an integer and __relationship_name__ is the name given to the relationship during the declaration process.

## Mantainers

Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>

## Changelog

### 0.5.24

* Use custom var to get wp-json url.

### 0.5.22

* Get metadata based on the related items, not the stored value.

### 0.5.21

* Add options for sidebar and icon.

### 0.5.19

* Add debounce to sidebar.

### 0.5.17

* Allow to create entry directly in search selectors if no answer while using the UI.

### 0.5.13

* Allow to dipslay list of related elements in admin.

### 0.5.10

* Solve bugs.

### 0.5.9

* Improve documentation.

### 0.5.8

* Improve cursor styles in the sorting system.

### 0.5.7

* Allow in administration to relate drafts, privates and future posts.

### 0.5.6

* Style adjustment for custom metaboxes.

### 0.5.5

* Add documentation for developers.

### 0.5.4

* [Bug] Recover styles for lists with unique member.

### 0.5.2

* Adding hooks to filter mysql clauses and postmeta value before save/update
* p2p\query_clauses($clauses, $relationship) : executed if it is a wp_p2p query
* p2p\before_update_postmeta($value, $key, $post_id) : executed before saving the postmeta.

### 0.5.0

* Use react-easy-sort

### 0.4.12

* Clean code

### 0.4.11

* Include option only_parents for relationship declaration

### 0.4.10

* Bug: Query not working when using tax_query

### 0.4.9

* Methods to query the relationships

### 0.4.8

* Include query with connected_type and connected_items

### 0.4.7

* Bug fixing

### 0.4.6

* Include UI option: maual o auto (default)

### 0.4.5

* Reciprocal relation in case of cardinality "one"

### 0.4.4

* Allow to relate posts of the same type, but cardinality has to be the same
* If it relates same type posts, display only the first UI.

### 0.4.3

* Custom db table.
* Copy data from p2p (old plugin) table 

### 0.4.2

* Adding dependencies from assets file. 

### 0.4.1

* Upgrading from p2p (plugin) environment. 

### 0.4

* Adding UI 

### 0.3

* In case of using this class in multiple providers, allow Composer to set which file to use by default.

### 0.2

* Allow to use PHP8.0

### 0.1

* First stable release
