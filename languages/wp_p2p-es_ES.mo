��          L      |       �      �   
   �      �      �   �   �   n  _     �     �     �     �  �   �                                         Create entry No options Search Searching... This is supposed to be a new installation, but the relartionships table exists. Please create a backup and delete the tables to continue. Project-Id-Version: wp_p2p
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-02-24 20:20-0500
Last-Translator: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language-Team: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;esc_attr_e;esc_attr__
X-Poedit-Basepath: ..
X-Generator: Poedit 3.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: src
X-Poedit-SearchPath-1: build
X-Poedit-SearchPath-2: template
 Crear entrada No hay opciones Buscar Buscando... Se supone que es una instalación nueva, pero la tabla de relaciones existe. Cree una copia de seguridad y elimine las tablas para continuar. 