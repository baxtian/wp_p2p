<?php
namespace Baxtian;

use Timber\Timber;

/**
 * Administrar los plugins requeridos
 */
class WP_P2P_Twig
{
	private $wp_p2p;

	/**
	 * Inicializa el componente
	 */
	public function __construct($wp_p2p)
	{
		// Filtros propios para ser llamados en twigs
		add_filter('p2p/relationships', [$this, 'p2p_relationships'], 10, 2);
		add_filter('p2p/metabox', [$this, 'p2p_metabox'], 10, 2);

		$this->wp_p2p = $wp_p2p;
	}

	/**
	 * Listado de elementos relacionados a partir de la relación 'relationship' con el elemento tag
	 * Para usar en tibmer: post|apply_filters('p2p/relationships', 'post_to_page')
	 */
	public function p2p_relationships($tag, $relationship)
	{
		$id = (is_numeric($tag)) ? $tag : $tag->id;

		// Argumentos para la consulta
		$args = [
			'post_type' => 'any',
			'p2p'       => [
				'relation' => 'AND',
				[
					'type' => $relationship,
					'to'   => $id,
				],
			],
			'nopaging' => true,
		];

		$posts = Timber::get_posts($args);

		return $posts;
	}

	private function meta_value_filter($item)
	{
		return $item->ID;
	}

	/**
	 * Listado de elementos relacionados a partir de la relación 'relationship' con el elemento tag
	 * Para usar en timer: {% do action('p2p/metabox', post.id, 'post_to_page') %}
	 */
	public function p2p_metabox($id, $relationship)
	{
		$this->pre_metabox();

		$relationships = (array) Timber::get_posts($this->wp_p2p->relationships($relationship, $id));
		$opposite      = $this->wp_p2p->opposite($relationship, $id);
		$meta          = array_map([$this, 'meta_value_filter'], $relationships);

		$args = [
			'id'            => $id,
			'relationship'  => $relationship,
			'relationships' => $relationships,
			'cardinality'   => $opposite['cardinality'],
			'allow_create'  => $opposite['allow_create'],
			'meta'          => '[' . join(',', $meta) . ']',
		];
		$filename = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'template' . DIRECTORY_SEPARATOR . 'metabox.twig';
		Timber::render($filename, $args);
	}

	private function pre_metabox()
	{
		// Directorio de assets
		$wp_component = basename(WP_CONTENT_URL);
		$path         = dirname(__DIR__);
		$_path        = explode($wp_component, $path);
		$url          = content_url(str_replace(DIRECTORY_SEPARATOR, '/', $_path[1])) ;

		// Archivo de dependencias
		$asset_file   = include($path . '/build/script-metabox.asset.php');
		$dependencies = array_unique(array_merge($asset_file['dependencies'], ['jquery', 'jquery-ui-sortable', 'easyautocomplete']));

		// Register autocomplete
		wp_register_style('easyautocomplete', $url . '/build/libs/easyautocomplete/easy-autocomplete.css', [], $asset_file['version']);
		wp_register_script('easyautocomplete', $url . '/build/libs/easyautocomplete/jquery.easy-autocomplete.js', ['jquery'], $asset_file['version']);

		// Registrar p2p-metabox
		wp_register_style('p2p_metabox', $url . '/build/style-metabox.css', ['easyautocomplete'], $asset_file['version']);
		wp_register_script('p2p_metabox', $url . '/build/script-metabox.js', $dependencies, $asset_file['version']);
		wp_localize_script('p2p_metabox', 'p2p_metabox', ['wp_json' => get_rest_url()]);

		// Registrar script box
		wp_enqueue_style('p2p_metabox');
		wp_enqueue_script('p2p_metabox');
	}

}
