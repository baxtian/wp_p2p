<?php

namespace Baxtian;

use WP_REST_Response;
use WP_Query;

/**
 * WpJSON para las sugerencias
 */
class WP_P2P_JSON
{
	use \Baxtian\Singleton;

	private $wp_p2p;

	/**
	 * Inicializa el componente
	 */
	public function __construct($wp_p2p)
	{
		add_action('rest_api_init', [$this, 'api']);
		add_action('wp_ajax_relationships', [$this, 'relationships_ajax']);

		$this->wp_p2p = $wp_p2p;
	}

	/**
	 * Función para registrar la consulta via api
	 */
	public function api()
	{
		register_rest_route('p2p/v1', 'relationships', [
			'methods'             => 'GET',
			'callback'            => [$this, 'relationships_api'],
			'permission_callback' => function () {
				return true; //current_user_can('edit_posts');
			},
			'args' => [
				'name' => [
					'default'           => '',
					'sanitize_callback' => 'sanitize_title',
				],
				'id' => [
					'default'           => '',
					'sanitize_callback' => 'absint',
				],
			],
		]);

		register_rest_route('p2p/v1', 'post', [
			'methods'             => 'GET',
			'callback'            => [$this, 'get_post_api'],
			'permission_callback' => function () {
				return true; //current_user_can('edit_posts');
			},
			'args' => [
				'id' => [
					'default'           => '',
					'sanitize_callback' => 'absint',
				],
			],
		]);

		register_rest_route('p2p/v1', 'options', [
			'methods'             => 'GET',
			'callback'            => [$this, 'options_api'],
			'permission_callback' => function () {
				return true; //current_user_can('edit_posts');
			},
			'args' => [
				'name' => [
					'default'           => '',
					'sanitize_callback' => 'sanitize_title',
				],
				'id' => [
					'default'           => '',
					'sanitize_callback' => 'absint',
				],
				'text' => [
					'default'           => '',
					'sanitize_callback' => 'sanitize_text_field',
				],
				'not__in' => [
					'default'           => '[]',
					'sanitize_callback' => 'sanitize_text_field',
				],
				'eac' => [
					'default'           => 0,
					'sanitize_callback' => 'absint',
				],
			],
		]);

		register_rest_route('p2p/v1', 'add_relationship', [
			'methods'             => 'POST',
			'callback'            => [$this, 'add_relationship_api'],
			'permission_callback' => function () {
				return true; //current_user_can('edit_posts');
			},
			'args' => [
				'name' => [
					'default'           => '',
					'sanitize_callback' => 'sanitize_title',
				],
				'from' => [
					'default'           => 0,
					'sanitize_callback' => 'absint',
				],
				'to' => [
					'default'           => 0,
					'sanitize_callback' => 'absint',
				],
			],
		]);

		register_rest_route('p2p/v1', 'remove_relationship', [
			'methods'             => 'POST',
			'callback'            => [$this, 'remove_relationship_api'],
			'permission_callback' => function () {
				return true; //current_user_can('edit_posts');
			},
			'args' => [
				'name' => [
					'default'           => '',
					'sanitize_callback' => 'sanitize_title',
				],
				'from' => [
					'default'           => 0,
					'sanitize_callback' => 'absint',
				],
				'to' => [
					'default'           => 0,
					'sanitize_callback' => 'absint',
				],
			],
		]);

		register_rest_route('p2p/v1', 'order_relationships', [
			'methods'             => 'GET',
			'callback'            => [$this, 'order_relationships_api'],
			'permission_callback' => function () {
				return true; //current_user_can('edit_posts');
			},
			'args' => [
				'name' => [
					'default'           => '',
					'sanitize_callback' => 'sanitize_title',
				],
				'from' => [
					'default'           => 0,
					'sanitize_callback' => 'absint',
				],
				'to' => [
					'default'           => 0,
					'sanitize_callback' => [$this, 'sanitize_array'],
				],
			],
		]);

		register_rest_route('p2p/v1', 'create_opposite_entry/(?P<relationship>[a-zA-Z0-9-_]+)/(?P<id>\d+)', [
			'methods'             => 'POST',
			'callback'            => [$this, 'create_opposite_entry_api'],
			'permission_callback' => function () {
				return true; //current_user_can('edit_posts');
			},
			'args' => [
				'name' => [
					'default'           => '',
					'sanitize_callback' => 'sanitize_text_field',
				],
				'id' => [
					'default'           => 0,
					'sanitize_callback' => 'absint',
				],
				'relationship' => [
					'default'           => '',
					'sanitize_callback' => 'sanitize_text_field',
				],
			],
		]);
	}

	/**
	 * Función para hacer la consulta via api
	 * @param  WP_REST_Request $request Datos de la solicitud
	 * @return WP_REST_Response         WP_REST_Response con la respuesta generada
	 */
	public function relationships_api($request)
	{
		$name = $request->get_param('name');
		$id   = $request->get_param('id');

		$answer = $this->relationships($name, $id);

		return new WP_REST_Response($answer, 200);
	}

	/**
	 * Función para hacer la consulta via api
	 * @param  WP_REST_Request $request Datos de la solicitud
	 * @return WP_REST_Response         WP_REST_Response con la respuesta generada
	 */
	public function get_post_api($request)
	{
		$id = $request->get_param('id');

		$answer = $this->get_post($id);

		return new WP_REST_Response($answer, 200);
	}

	private function eac_result($item)
	{
		return [
			'name' => $item->post_title,
			'id'   => $item->ID,
		];
	}

	/**
	 * Buscar estructuras que cumlan la relación 'relationship' desde 'from' yque tengan el texto 'text'.
	 * @param  WP_REST_Request $request Datos de la solicitud
	 * @return WP_REST_Response         WP_REST_Response con la respuesta generada
	 */
	public function options_api($request)
	{
		$name    = $request->get_param('name');
		$id      = $request->get_param('id');
		$text    = $request->get_param('text');
		$not__in = $request->get_param('not__in');
		$eac     = ($request->get_param('eac') != 0) ? true : false;

		// Si es consulta para los selectores, hacer la consulta completa
		if ($eac) {
			$answer = $this->options($name, $id, $text);

			// Filtrar
			$answer = array_map([$this, 'eac_result'], $answer);

			// Si no hubo respuestas determinar si este
			// listado debe incluir opción para crear
			if(count($answer) == 0) {
				$opposite = $this->wp_p2p->opposite($name, $id);

				// Si esta extremo de la relación permite crear
				if($opposite['allow_create']) {
					// Agregar la opción -1 para indicar que
					// esta entrada se puede crear
					$answer = [
						[
							'name' => $text,
							'id'   => -1,
						],
					];
				}
			}

			$answer = [
				'items' => $answer,
				'query' => $text,
			];
		} else {
			// Si no es para selectores, hacer la consulta sin
			// los not__in

			// Organizar not__in
			$not__in = str_replace(['[', ']'], '', $not__in);
			$not__in = array_map('intval', explode(',', $not__in));

			$answer = $this->options($name, $id, $text, $not__in);
		}

		return new WP_REST_Response($answer, 200);
	}

	/**
	 * Función para agregar relación via api
	 * @param  WP_REST_Request $request Datos de la solicitud
	 * @return WP_REST_Response         WP_REST_Response con la respuesta generada
	 */
	public function add_relationship_api($request)
	{
		$name = $request->get_param('name');
		$from = $request->get_param('from');
		$to   = $request->get_param('to');

		$answer = $this->add_relationship($name, $from, $to);

		return new WP_REST_Response($answer, 200);
	}

	/**
	 * Función para eliminar relación via api
	 * @param  WP_REST_Request $request Datos de la solicitud
	 * @return WP_REST_Response         WP_REST_Response con la respuesta generada
	 */
	public function remove_relationship_api($request)
	{
		$name = $request->get_param('name');
		$from = $request->get_param('from');
		$to   = $request->get_param('to');

		$answer = $this->remove_relationship($name, $from, $to);

		return new WP_REST_Response($answer, 200);
	}

	/**
	 * Función para agregar relación via api
	 * @param  WP_REST_Request $request Datos de la solicitud
	 * @return WP_REST_Response         WP_REST_Response con la respuesta generada
	 */
	public function order_relationships_api($request)
	{
		$name = $request->get_param('name');
		$from = $request->get_param('from');
		$to   = $request->get_param('to');

		$answer = $this->order_relationships($name, $from, $to);

		return new WP_REST_Response($answer, 200);
	}

	/**
	 * Función para crear una entrada
	 * @param  WP_REST_Request $request Datos de la solicitud
	 * @return WP_REST_Response         WP_REST_Response con la respuesta generada
	 */
	public function create_opposite_entry_api($request)
	{
		$name         = $request->get_param('name');
		$id           = $request->get_param('id');
		$relationship = $request->get_param('relationship');

		$answer = $this->create_opposite_entry($name, $id, $relationship);

		return new WP_REST_Response($answer, 200);
	}

	/**
	 * Función para hacer la consulta via Ajax
	 * @return string JSON con la respuesta generada.
	 */
	public function relationships_ajax()
	{
		$answer = [];

		if (isset($_GET['name']) && isset($_GET['id'])) {
			$name = sanitize_text_field($_GET['name']);
			$id   = sanitize_text_field($_GET['id']);
		} else {
			wp_die(0);
		}

		$answer = $this->relationships($name, $id);

		echo json_encode($answer);
		wp_die();
	}

	/**
	 * Consulta las relaciones de tipo 'name' con post 'id'
	 * @param  string  $name            Relacionamiento
	 * @param  string  $id              Id de la entrada
	 * @return array                    Arreglo con los datos de la cantidad de items y la lista
	 *                                  de entradas que cumplen con el criterio de búsqueda.
	 */
	private function relationships($name, $id)
	{
		return $this->wp_p2p->relationships($name, $id);
	}

	/**
	 * Consulta las opciones que puede tener una relacion tip 'relationship' desde from y que tengan el texto 'text'
	 * @param  string  $name		Relacionamiento
	 * @param  int     $id          Id de la entrada
	 * @param  string  $text		Texto de las estructuras
	 * @return array                Arreglo con los datos de la cantidad de items y la lista
	 *                              de entradas que cumplen con el criterio de búsqueda.
	 */
	private function options($name, $id, $text, $not__in = [])
	{
		// Datos de la relación
		$relationship = $this->wp_p2p->relation_data($name);

		// Buscar el tipo del que se están buscando opciones para relacionar
		$from_post_type = get_post_type($id);

		// Si no hay relación de este tipo o no existe el post según el id, retornar vacía
		if (!$relationship || !$from_post_type) {
			return [];
		}

		// Determionar el extremo contrario de la relación
		if ($relationship['from']['type'] == $from_post_type) {
			$to_post = $relationship['to'];
		} else {
			$to_post = $relationship['from'];
		}

		// Argumentos de búsqueda
		$args = [
			'post_type'    => $to_post['type'],
			'post__not_in' => $not__in,
			'post_status'  => 'any',
			's'            => $text,
		];

		// Determinar si solo se mostrarán los del primer nivel
		if($to_post['only_parents']) {
			$args['post_parent'] = 0;
		}

		// Solo título
		add_filter('posts_search', [$this, 'solo_titulo'], 500, 2);

		$query = new WP_Query($args);

		return $query->posts;
	}

	/**
	 * Filtro para hacer la consulta solo por títutlo
	 * @param  string   $search   Regla SQL que corresponde a los elemntos a buscar
	 * @param  WP_Query $wp_query Componente para consutas a la base de datos.
	 * @return string             La nueva regla SQL con la consulta extra.
	 */
	public function solo_titulo($search, $wp_query)
	{
		global $wpdb;

		// Solo intentar si estamos buscando algo.
		if (empty($search)) {
			return $search;
		}

		// Extraer las variables de búsqeuda
		$q = $wp_query->query_vars;
		$n = ! empty($q['exact']) ? '' : '%';

		// Inicializar variables para búsquedaa y el concatenador.
		// El primero será sin concatenación, los siguientes si tendrán la concatenación al inicio.
		$search = $searchand = '';

		// Por cada término que se esté buscando agregamos la restricción
		foreach ((array) $q['search_terms'] as $term) {
			// Sanear el search para evitar que inserten código por el script.
			$term = esc_sql($wpdb->esc_like($term));

			// COncatenar una consulta que busque en el título
			$search .= "{$searchand}($wpdb->posts.post_title LIKE '{$n}{$term}{$n}')";

			// El próximo si se concatenará con AND
			$searchand = ' AND ';
		}

		// En caso de estar logeado, usar solo las notas que no requieran clave de acceso
		if (! empty($search)) {
			$search = " AND ({$search}) ";
			if (! is_user_logged_in()) {
				$search .= " AND ($wpdb->posts.post_password = '') ";
			}
		}

		// Retornar la nueva consulta.
		return $search;
	}

	/**
	 * Agrega una relación de tipo 'name' entre dos estructuras.
	 * @param  string  $name            Relacionamiento
	 * @param  string  $from            Id de la entrada from
	 * @param  string  $to              Id de la entrada to
	 * @return array                    Arreglo con los datos de la cantidad de items y la lista
	 *                                  de entradas que cumplen con el criterio de búsqueda.
	 */
	private function add_relationship($name, $from, $to)
	{
		// Arreglo que contendrá los resultados
		$answer = [];

		$this->wp_p2p->add_relation($name, $from, $to);

		return $this->relationships($name, $from);
	}

	/**
	 * Elimina la relación de tipo 'name' entre dos estructuras.
	 * @param  string  $name            Relacionamiento
	 * @param  string  $from            Id de la entrada from
	 * @param  string  $to              Id de la entrada to
	 * @return array                    Arreglo con los datos de la cantidad de items y la lista
	 *                                  de entradas que cumplen con el criterio de búsqueda.
	 */
	private function remove_relationship($name, $from, $to)
	{
		// Arreglo que contendrá los resultados
		$answer = [];

		$this->wp_p2p->remove_relation($name, $from, $to);

		return $this->relationships($name, $from);
	}

	/**
	 * Ordena la lista de tipo 'name' from -> [to]
	 * @param  string  $name            Relacionamiento
	 * @param  string  $from            Id de la entrada from
	 * @param  string  $to              Id de la entrada to
	 * @return array                    Arreglo con los datos de la cantidad de items y la lista
	 *                                  de entradas que cumplen con el criterio de búsqueda.
	 */
	private function order_relationships($name, $from, $to)
	{
		// Arreglo que contendrá los resultados
		$answer = [];

		$this->wp_p2p->set_relation($name, $from, $to);

		return $this->relationships($name, $from);
	}

	/**
	 * Obtener datos de un item
	 *
	 * @param int $id
	 * @return mixed
	 */
	public function get_post($id)
	{
		return $this->wp_p2p->get_post($id);
	}

	/**
	 * Sanitiza una entrada de comas separadas en un arreglo
	 * @param  string  $value           Relacionamiento
	 * @return array                    Arreglo con los datos enteros sanitizados.
	 */
	public function sanitize_array($values)
	{
		$multi_values = !is_array($values) ? explode(',', $values) : $values;

		return !empty($multi_values) ? array_map('absint', $multi_values) : false;
	}

	/**
	 * Función apra crear una entrada
	 *
	 * @param string 	$name
	 * @param int 		$id
	 * @param string 	$relationship
	 * @return mixed
	 */
	public function create_opposite_entry(string $name, int $id, string $relationship)
	{
		$opposite = $this->wp_p2p->opposite($relationship, $id);

		if(!$opposite || !$opposite['allow_create']) {
			return false;
		}

		$my_post = [
			'post_title'   => sanitize_text_field($name),
			// 'post_content' => sanitize_text_field($name),
			'post_status'  => 'publish',
			'post_author'  => get_current_user_id(),
			'post_type'    => sanitize_text_field($opposite['type']),
		];

		// Insert the post into the database
		$id = wp_insert_post($my_post);

		return $this->wp_p2p->get_post($id);
	}
}
