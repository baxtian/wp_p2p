<?php
namespace Baxtian;

/**
 * Administrar los plugins requeridos
 */
class WP_P2P_Query
{
	/**
	 * Declarar acciones y filtros para WP_Query
	 *
	 * @return void
	 */
	public static function init()
	{
		add_action('parse_query', [__CLASS__, 'parse_query'], 20);
		add_filter('posts_clauses', [__CLASS__, 'posts_clauses'], 20, 2);
		add_filter('query_vars', [__CLASS__, 'add_query_vars']);
	}

	public static function add_query_vars($qv)
	{
		$qv[] = 'p2p';

		return $qv;
	}

	/**
	 * Parsear los datos de esta consulta
	 *
	 * @param object $wp_query
	 * @return void
	 */
	public static function parse_query($wp_query)
	{
		// ¿Está dentro de la consulta ('p2p') y ('connected_type' o 'connected_items')?
		if (
			!isset($wp_query->query_vars['p2p']) &&
			(
				!isset($wp_query->query_vars['connected_type']) ||
				!isset($wp_query->query_vars['connected_items'])
			)
		) {
			return;
		}

		// Si no está 'p2p' entoces crearla a partir de connected_type y connected_items
		// Y asignar post_type como any
		$r = [];
		if(!isset($wp_query->query_vars['p2p'])) {
			$r = [
				'relation' => 'AND',
				[
					'type' => $wp_query->query_vars['connected_type'],
					'to'   => $wp_query->query_vars['connected_items'],
				],
			];
			$wp_query->query_vars['post_type'] = 'any';
		} else {
			$r = $wp_query->query_vars['p2p'];
		}

		// Si no hay relación declarar como 'o'
		if (!isset($r['relation'])) {
			$r['relation'] = 'OR';
		} else {
			$r['relation'] = strtoupper($r['relation']);
		}

		$wp_query->_p2p_query = $r;

		// Eliminar supress_filer y sticky_posts
		// Si dejamos estas opciones podría mostrar
		// elementos que no hacen parte de la consulta
		// por estar almacenados como elementos sticky
		$wp_query->query_vars = array_merge($wp_query->query_vars, [
			'suppress_filters'    => false,
			'ignore_sticky_posts' => true,
		]);

		// Evitar que la consulta sea interpretada como
		// inicio o como archivo
		$wp_query->is_home    = false;
		$wp_query->is_archive = true;
	}

	public static function posts_clauses($clauses, $wp_query)
	{
		// Base de datos y tabla
		global $wpdb;
		$table = $wpdb->prefix . 'p2p_posts';

		// Si no tenemos consultas de p2p, retornar la clausula tal cual
		if (!isset($wp_query->_p2p_query)) {
			return $clauses;
		}

		// Inicializar el arreglo de consultas 'where'
		$where_parts   = [];
		$orderby_parts = [];

		// Si la relación es un 'or'
		if ($wp_query->_p2p_query['relation'] == 'OR') {

			// Agregar los join genéricos tanto para los from como
			// para los to.

			// Tabla 'to' es la que tiene en el 'from' el elemento
			// que estamos buscando
			$clauses['join'] .= " INNER JOIN $table AS tp2p_from ON {$wpdb->posts}.ID=tp2p_from.p2p_to";

			// Tabla 'from' es la que tiene en el 'to' el elemento
			// que estamos buscando
			$clauses['join'] .= " INNER JOIN $table AS tp2p_to ON {$wpdb->posts}.ID=tp2p_to.p2p_from";

			// Recorrer las relaciones
			foreach ($wp_query->_p2p_query as $relation) {
				// Si es un arreglo, crear un where
				// El caso contrario es el tipo de relacionamiento
				// de las consultas y no se necesita por lo tanto usar
				// en el generador de las consultas 'where'.
				if (is_array($relation)) {
					// Determinar el nombre de la relación
					$key = $relation['type'];

					// Si hay consulta 'to' en este relacionamiento
					if (isset($relation['to'])) {
						// Determinar id de relación tipo 'to' que
						// por lo tanto es de tipo ->id
						$to = $relation['to'];

						// Incluir la consulta: buscar en en la tabla 'to'
						// el id consultado (->id) y que el tipo sea el
						// indicado
						$where_parts[] = "(tp2p_to.p2p_to IN ({$to}) AND tp2p_to.p2p_type = '{$key}')";
					} elseif (isset($relation['from'])) { // Si hay consulta 'from'
						// Determinar id de relación tipo 'from' que
						// por lo tanto es de tipo id->
						$from = $relation['from'];

						// Incluir la consulta: buscar en en la tabla 'from'
						// el id consultado (id->) y que el tipo sea el
						// indicado
						$where_parts[] = "(tp2p_from.p2p_from IN ({$from}) AND tp2p_from.p2p_type = '{$key}')";
					}
				}
			}
			// Unirlas según relacionamiento
			$clauses['where'] .= sprintf(' AND (%s)', join(" {$wp_query->_p2p_query['relation']} ", $where_parts));
		} else { // La relación es un AND
			// Recorrer las relaciones para ir creando las tablas
			// y consultas para cada una
			$i = 1;
			foreach ($wp_query->_p2p_query as $relation) {
				// Si es un arreglo, crear el 'where' y el 'join'
				// El caso contrario es el tipo de relacionamiento
				// de las consultas y no se necesita por lo tanto usar
				// en el generador de las consultas 'where' y 'join'.
				if (is_array($relation)) {
					$key = $relation['type'];
					// Si tiene la consulta tipo 'to'
					if (isset($relation['to'])) {
						// Incluir el join del tipo 'to' en la tabla del actual índice
						// Tabla 'to' es la que tiene en el 'from' el elemento
						// que estamos buscando
						$clauses['join'] .= " INNER JOIN $table AS tp2p{$i} ON {$wpdb->posts}.ID=tp2p{$i}.p2p_from";

						// Determinar id de relación tipo 'to' ->id
						$to = $relation['to'];

						// Incluir la consulta: que en la tabla del join identificada
						// con el mismo índice esté el id consultado (->id) y que el
						// tipo sea el indicado
						$where_parts[]   = "(tp2p{$i}.p2p_to IN ({$to}) AND tp2p{$i}.p2p_type = '{$key}')";
						$orderby_parts[] = "tp2p{$i}.p2p_order ASC";
					} elseif (isset($relation['from'])) {
						// Incluir el join del tipo 'from' en la tabla del actual índice
						// Tabla 'from' es la que tiene en el 'to' el elemento
						// que estamos buscando
						$clauses['join'] .= " INNER JOIN $table AS tp2p{$i} ON {$wpdb->posts}.ID=tp2p{$i}.p2p_to";

						// Determinar id de relación tipo 'from' id->
						$from = $relation['from'];

						// Incluir la consulta: que en la tabla del join identificada
						// con el mismo índice esté el id consultado (id->) y que el
						// tipo sea el indicado
						$where_parts[]   = "(tp2p{$i}.p2p_from IN ({$from}) AND tp2p{$i}.p2p_type = '{$key}')";
						$orderby_parts[] = "tp2p{$i}.p2p_order ASC";
					}

					// Incrementar el índice de tablas
					$i++;
				}
			}
			// Unirlas según relacionamiento
			$clauses['where'] .= sprintf(' AND (%s)', join(" {$wp_query->_p2p_query['relation']} ", $where_parts));
		}

		// Agrupar para evitar repeticiones
		$clauses['groupby'] = "{$wpdb->posts}.ID";

		// Orderby
		$clauses['orderby'] = implode(' AND ', $orderby_parts);

		// Filtro de cláusulas
		$clauses = apply_filters('p2p/query_clauses', $clauses, $wp_query->_p2p_query);

		return $clauses;
	}
}
