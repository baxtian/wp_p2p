<?php

namespace Baxtian;

use Baxtian\WP_P2P;

/**
 * Columnas de administración
 */
class WP_P2P_AdminColumns
{
	private $p2p;
	private $relationship;
	private $edge;
	private $contrary;

	/**
	 * Inicializa del componente de la columna de administración
	 */
	public function __construct(WP_P2P $p2p, array $relationship, string $edge)
	{
		// Vincular valores
		$this->p2p          = $p2p;
		$this->relationship = $relationship;
		$this->edge         = $edge;
		$this->contrary     = ($edge == 'from') ? 'to' : 'from';

		// Archivo de dependencias
		// Directorio de assets
		$wp_component = basename(WP_CONTENT_URL);
		$path         = dirname(__DIR__);
		$_path        = explode($wp_component, $path);
		$url          = content_url(str_replace(DIRECTORY_SEPARATOR, '/', $_path[1])) ;

		$asset_file = include($path . '/build/script-side.asset.php');

		wp_register_style('p2p_admin', $url . '/build/admin.css', [], $asset_file['version']);

		// Registrar estilos admin
		wp_enqueue_style('p2p_admin');

		// Columnas para relación en el tipo contrario
		add_filter("manage_edit-{$this->relationship[$this->edge]['type']}_columns", [$this, 'columns']);
		add_action("manage_{$this->relationship[$this->edge]['type']}_posts_custom_column", [$this, 'content'], 10, 2);
	}

	/**
	 * Agrega la columna para la relación a ser vista en el
	 * listado de la estructura contraria del administrador
	 *
	 * @param array $columns
	 * @return array
	 */
	public function columns($columns)
	{
		// Agregar la columna de la relación en la posición indicada
		$position = $this->relationship[$this->contrary]['display_in_admin_column'];
		$part1    = array_slice($columns, 0, $position);
		$part2    = array_slice($columns, $position);
		$part3    = [$this->relationship['name'] => $this->relationship[$this->contrary]['title']];

		return array_merge($part1, $part3, $part2);
	}

	/**
	 * Determina el contenido a mostrar en la columna
	 *
	 * @param string $column_name
	 * @param int $post_id
	 * @return void
	 */
	public function content($column_name, $post_id)
	{
		// Si el nombre dela columna es el de la relación
		if($column_name == $this->relationship['name']) {
			// Dterminar la lista de relaciones
			$posts = $this->p2p->relationships($this->relationship['name'], $post_id);

			// Si hay al menos un item en la relación, visualizarla
			if (count($posts) > 0) {
				$map = array_map([$this, 'link'], $posts);

				// Si hay más de un item en la relación, indicar la cantidad
				if(count($posts) > 1) {
					array_unshift($map, sprintf("<span class='p2p_admin_list_count'>%d</span>", count($posts)));
				}
				echo implode(' | ', $map);
			};
		}
	}

	/**
	 * Retorna el enlace al post
	 *
	 * @param object $post
	 * @return string
	 */
	private function link(object $post)
	{
		return sprintf(
			"<span class='p2p_admin_list_item'><a href='%s'>%s</a></span>",
			get_edit_post_link($post),
			$post->post_title
		);
	}
}
