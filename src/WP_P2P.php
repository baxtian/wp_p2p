<?php

namespace Baxtian;

use Baxtian\WP_P2P_UI;
use Baxtian\WP_P2P_Twig;
use Baxtian\WP_P2P_Query;
use Baxtian\WP_P2P_JSON;
use Baxtian\WP_P2P_AdminColumns;

/**
 * Administrar los plugins requeridos
 */
class WP_P2P
{
	private $relationships;
	private $types;

	/**
	 * Constructor para crear una instancia
	 */
	protected function __construct($file)
	{
		// Inicializar arreglo de relaciones
		$this->relationships = [];
		$this->types         = [];

		// Inicializar traducción
		add_action('admin_init', [$this, 'i18n']);

		// Función de activación:
		// * crear base de datos
		register_activation_hook($file, [$this, 'activate']);

		// Crear los hooks
		add_action('wp_loaded', [$this, 'connections']);

		// Inicializar en el rest api las variables que son quick edit
		add_action('init', [$this, 'init']);

		// Inicializar mecanismos para WP_Query, WP_JSON y Twig
		WP_P2P_Query::init();
		new WP_P2P_JSON($this);
		new WP_P2P_Twig($this);
	}

	public function __get($name)
	{
		switch($name) {
			case 'relationships':
				return $this->relationships;

				break;
		}

		return null;
	}

	public function init()
	{
		// Inicializar en el rest api las variables que son quick edit
		add_action('rest_api_init', [$this, 'api'], 99);

		add_filter('add_post_metadata', [$this, 'save_meta'], 20, 5);
		add_filter('update_post_metadata', [$this, 'save_meta'], 20, 5);
		add_filter('get_post_metadata', [$this, 'get_meta'], 20, 4);
	}

	/**
	 * Importar el archivo de traducción
	 * @codeCoverageIgnore
	 */
	public function i18n()
	{
		$domain = 'wp_p2p';
		$locale = apply_filters('plugin_locale', determine_locale(), $domain);

		$wp_component = basename(WP_CONTENT_URL);
		$_path        = explode($wp_component, __DIR__);
		$path         = dirname(WP_CONTENT_DIR . $_path[1]) . '/languages/';
		$mofile       = $path . $domain . '-' . $locale . '.mo';

		load_textdomain($domain, $mofile);
	}

	/**
	 * Función para inicializar la base de datos
	 *
	 * @return void
	 */
	public function activate()
	{
		global $wpdb;

		$table_name     = $wpdb->prefix . 'p2p_posts';
		$p2p_table_name = $wpdb->prefix . 'p2p';

		$charset_collate = $wpdb->get_charset_collate();

		$db_version = get_option('wp_p2p_version');
		switch ($db_version) {
			case '1.0': //La versión actual. No hacer nada
				// update_option('cdc_db_version', CDC_DB_VERSION);
				break;
			default: //Instalación nueva, crear las tablas.
				// Tabla de relaciones
				// Crear la tabla solo si no existen aun
				if (
					$wpdb->get_var("show tables like '$table_name'") != $table_name
				) {
					$sql = "CREATE TABLE $table_name (
							p2p_id bigint(20) unsigned NOT NULL auto_increment,
							p2p_from bigint(20) unsigned NOT NULL,
							p2p_to bigint(20) unsigned NOT NULL,
							p2p_type varchar(60) NOT NULL default '',
							p2p_order int(5) NOT NULL default 0,
							PRIMARY KEY  (p2p_id),
							KEY p2p_from (p2p_from),
							KEY p2p_to (p2p_to),
							KEY p2p_type (p2p_type),
							UNIQUE KEY relation (p2p_from, p2p_to, p2p_type)
						) $charset_collate;";
					require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
					dbDelta($sql);

					update_option('wp_p2p_version', '1.0');

					// ¿Existe la tabla del plugin p2p?
					if ($wpdb->get_var("show tables like '$p2p_table_name'") == $p2p_table_name) {
						// Recorrer lista de relaciones y agregarlas en la nueva tabla
						$sql     = "SELECT * FROM $p2p_table_name";
						$results = $wpdb->get_results($sql);
						foreach ($results as $item) {
							// Ingresar el dato de la tabla|
							$wpdb->insert(
								$table_name,
								[
									'p2p_from' => $item->p2p_from,
									'p2p_to'   => $item->p2p_to,
									'p2p_type' => $item->p2p_type,
								],
								[
									'%d',
									'%d',
									'%s',
								]
							);

							// Ingresar el contrario
							$wpdb->insert(
								$table_name,
								[
									'p2p_from' => $item->p2p_to,
									'p2p_to'   => $item->p2p_from,
									'p2p_type' => $item->p2p_type,
								],
								[
									'%d',
									'%d',
									'%s',
								]
							);
						}
					}
				} else {
					$error_msg = __('This is supposed to be a new installation, but the relartionships table exists. Please create a backup and delete the tables to continue.', 'wp_p2p');
					trigger_error($error_msg, E_USER_ERROR);
				}

				break;
		}
	}

	protected function setup_edge(&$data, $edge)
	{
		// Si el 'edge' es una cadena, modificarla para la estructura base
		if (is_string($data[$edge])) {
			$data[$edge] = [
				'type' => $data[$edge],
			];
		}

		// Si no hay permiso para crear una instancia de la estructura
		// indicar que no es permitido por defecto
		if (!isset($data[$edge]['can_create_post'])) {
			$data[$edge]['can_create_post'] = false;
		}

		// Si hay atributo para ordenamiento desde edge
		if (isset($data[$edge]['sortable'])) {
			// Mantener el valor
		} elseif (isset($data['sortable'])) {
			// Si no hay valor asignado, pero hay atributo global
			// asignar ese valor global
			$data[$edge]['sortable'] = $data['sortable'];
		} else {
			// Usar false como valor por defecto
			$data[$edge]['sortable'] = false;
		}

		// Si hay atributo para ui desde edge
		if (isset($data[$edge]['ui'])) {
			// Mantener el valor
		} else {
			// Usar false como valor por defecto
			$data[$edge]['ui'] = 'auto';
		}

		// Si hay atributo para ui desde edge
		if (isset($data[$edge]['icon'])) {
			// Mantener el valor
		} else {
			// Usar false como valor por defecto
			$data[$edge]['icon'] = 'admin-links';
		}

		// Cardinalidad
		$cardinalities = explode('_', $data['cardinality']);
		$cardinality   = ($edge == 'to') ? $cardinalities[2] : $cardinalities[0];

		if ($cardinality == 'ONE') {
			$data[$edge]['cardinality'] = 'ONE';
		} else {
			$data[$edge]['cardinality'] = 'MANY';
		}

		// Título
		if (!isset($data[$edge]['title'])) {
			$aux                  = get_post_type_object($data[$edge]['type']);
			$data[$edge]['title'] = ($data[$edge]['cardinality'] == 'ONE') ? $aux->labels->singular_name : $aux->labels->name;
		}

		// ¿Solo usar padres para las opciones?
		$data[$edge]['only_parents'] = $data[$edge]['only_parents'] ?? false;

		// Si no marca para columna en administración, poner false
		if (!isset($data[$edge]['display_in_admin_column'])) {
			$data[$edge]['display_in_admin_column'] = false;
		}

		// Si no marca para permitir creación, poner false
		if (!isset($data[$edge]['allow_create'])) {
			$data[$edge]['allow_create'] = false;
		}
	}

	/**
	 * Registrar una relación
	 *
	 * @param array $data Datos de la relación
	 * @return void
	 */
	protected function register_relationship($data)
	{
		// Verificar que tenga los datos mínimos: name, from, to
		if (!isset($data['name']) || !isset($data['from']) || !isset($data['to'])) {
			return false;
		}

		// Indicar la reciprocidad por defecto
		if (!isset($data['reciprocal'])) {
			$data['reciprocal'] = true;
		}

		// Indicar la cardinalidad por defecto
		if (isset($data['from']['cardinality']) && isset($data['to']['cardinality'])) {
			$data['cardinality'] = $data['from']['cardinality'] . '_TO_' . $data['to']['cardinality'];
		} elseif (isset($data['from']['cardinality'])) {
			$data['cardinality'] = $data['from']['cardinality'] . '_TO_ANY';
		} elseif (isset($data['to']['cardinality'])) {
			$data['cardinality'] = 'ANY_TO_' . $data['to']['cardinality'];
		} elseif (!isset($data['cardinality'])) {
			$data['cardinality'] = 'ANY_TO_ANY';
		}
		$data['cardinality'] = strtoupper($data['cardinality']);

		/**** From */
		$this->setup_edge($data, 'from');

		/**** To */
		$this->setup_edge($data, 'to');

		// Si from y to son iguales, la cardinalidad debe serlo también
		if (
			$data['from']['type'] == $data['to']['type'] &&
			$data['from']['cardinality'] != $data['to']['cardinality']
		) {
			return false;
		}

		// Guardar la relación en el arreglo de relaciones
		$this->relationships[$data['name']] = $data;

		// Atender evento para guardar estas estructuras
		// evitando repetir
		$post_type_from = $data['from']['type'];
		if (!in_array($post_type_from, $this->types)) {
			add_action('save_post_' . $post_type_from, [$this, 'save']);
			$this->types[] = $post_type_from;
		}
		$post_type_to = $data['to']['type'];
		if (!in_array($post_type_to, $this->types)) {
			add_action('save_post_' . $post_type_to, [$this, 'save']);
			$this->types[] = $post_type_to;
		}

		// Si estamos en el escritorio, declarar la UI
		if (is_admin()) {
			if (in_array($data['from']['ui'], ['auto', 'sidebar'])) {
				new WP_P2P_UI($data['name'], 'from', $data);
			}

			// Si los tipos son distintos, crear el UI del sentido contrario
			if (in_array($data['to']['ui'], ['auto', 'sidebar']) && $data['from']['type'] != $data['to']['type']) {
				new WP_P2P_UI($data['name'], 'to', $data);
			}

			// Si hay columna asignada en from, activar la columna
			if($data['from']['display_in_admin_column'] !== false) {
				new WP_P2P_AdminColumns($this, $data, 'to');
			}

			if($data['to']['display_in_admin_column'] !== false) {
				new WP_P2P_AdminColumns($this, $data, 'from');
			}
		}
	}

	/**
	 * Valida que la lista 'to' pueda tener una relación de tipo 'type'
	 * con 'from'.  Retorna la lista de WP_Posts que si pueden
	 * tener una relación.
	 *
	 * @param string $type
	 * @param int $p1
	 * @param int $p2
	 * @return array
	 */
	private function validate($type, $p1, $p2)
	{
		// Buscar en la lista de relaciones el tipo de relación
		// Si no existe, indicar el error
		if (!isset($this->relationships[$type])) {
			return [];
		}

		// Estado de publicación
		$respuesta = [];

		// Guardar el tipo
		$type = $this->relationships[$type];

		// Obtener el post 'from'
		if (is_integer($p1)) {
			$p1 = get_post($p1);
		}

		// Si no es una estructura, retornar vacio
		if (!is_object($p1)) {
			return [];
		}

		// Convertir el segundo item en un arreglo
		if (is_integer($p2)) {
			$p2 = [$p2];
		}

		// Recorrer el arreglo de destino
		foreach ($p2 as $item) {
			// Obtener el post 'from'
			if (is_integer($item)) {
				$item = get_post($item);
			}

			// Revisar que sean los tipos esperados
			// o inertidos pero para relación recíproca
			if (
				is_object($item) &&
				(
					(
						$p1->post_type == $type['from']['type'] &&
						$item->post_type == $type['to']['type']
					) || (
						$type['reciprocal'] &&
						$item->post_type == $type['from']['type'] &&
						$p1->post_type == $type['to']['type']
					)
				)
			) {
				$respuesta[] = $item->ID;
			}
		}

		return $respuesta;
	}

	/**
	 * Añadir una relación entre 'from' y 'to' del tipo 'type'.
	 * Retorna true si pudo crear la relación o ya existía,
	 * false si no pudo crearla.
	 *
	 * @param string $type
	 * @param int  $p1
	 * @param int  $p2
	 * @param bool $keep_order
	 * @return bool
	 */
	public function add_relation($type, $p1, $p2)
	{
		// Agregar las relaciones sin forzar el orden indicado
		$this->_add_relation($type, $p1, $p2, false);
	}

	/**
	 * Añadir una relación entre 'from' y 'to' del tipo 'type'.
	 * Retorna true si pudo crear la relación o ya existía,
	 * false si no pudo crearla.
	 *
	 * @param string $type
	 * @param int  $p1
	 * @param int  $p2
	 * @param bool $keep_order
	 * @return bool
	 */
	private function _add_relation($type, $p1, $p2, $keep_order)
	{
		// Conección a base de datos
		global $wpdb;
		$table_name = $wpdb->prefix . 'p2p_posts';

		// post type
		$post_type = get_post_type($p1);
		$d1        = ($this->relationships[$type]['from']['type'] == $post_type) ? 'from' : 'to';
		$d2        = ($d1 == 'from') ? 'to' : 'from';

		// Validar la lista de relación y tener la lista de tipo WP_Post
		$p2 = $this->validate($type, $p1, $p2);

		$reciprocal = $this->relationships[$type]['reciprocal'];

		// Buscar el valor máximo de ordenamiento
		$sql   = "SELECT MAX( t1.p2p_order ) FROM {$table_name} as t1 WHERE t1.p2p_type = '{$type}' AND t1.p2p_from = {$p1}";
		$order = (int) $wpdb->get_var($sql);
		$order = (empty($order)) ? 0 : $order;

		// Buscar el valor máximo de ordenamiento para el recíproco en caso de ser necesario
		if ($reciprocal) {
			$sql       = "SELECT MAX( t1.p2p_order ) FROM {$table_name} as t1 WHERE t1.p2p_type = '{$type}' AND t1.p2p_to = {$p1}";
			$order_inv = (int) $wpdb->get_var($sql);
			$order_inv = (empty($order_inv)) ? 0 : $order_inv;
		}

		// Recorrer el arreglo de Insertar los datos de la relación p1-p2-type en la tabla
		foreach ($p2 as $item) {
			// ¿Existe ya la relación?
			$rowcount = $wpdb->get_var("
					SELECT COUNT(*) 
					FROM $table_name 
					WHERE
						p2p_from = $p1 AND
						p2p_to = $item AND
						p2p_type = '$type'
					");

			// Determinar si es ordenable y según si estamos pidiendo que se mantenga el orden
			$set_order = 0;
			if ($this->relationships[$type][$d1]['sortable']) {
				// Si no está marcado para seguir el orden estricto
				if (!$keep_order) {
					// Incrementar el dato de ordenamiento
					$order++;
					$set_order = $order;
				} else { // Ok, el orden debe ser tal cual el indicado
					$keep_order = (int) $keep_order;
					$set_order  = $keep_order;
					$keep_order++;
				}
			}

			// Si es recíproco insertar en el otro sentido
			if ($reciprocal) {
				// ¿Es el contrario es cardinalidad ONE?
				// Si es así eliminar las conecciones previas
				if ($this->relationships[$type][$d1]['cardinality'] == 'ONE') {
					$wpdb->delete(
						$table_name,
						[
							'p2p_from' => $item,
							'p2p_type' => $type,
						],
						[
							'%d',
							'%s',
						]
					);

					$wpdb->delete(
						$table_name,
						[
							'p2p_to'   => $item,
							'p2p_type' => $type,
						],
						[
							'%d',
							'%s',
						]
					);
				}

				// ¿Existe ya la relación?
				$rowcount2 = $wpdb->get_var("
						SELECT COUNT(*) 
						FROM $table_name 
						WHERE
							p2p_from = $item AND
							p2p_to = $p1 AND
							p2p_type = '$type'
						");

				// Si no existe, insertarlo
				if ($rowcount2 == 0) {
					// ¿Es ordenable?
					$set_order = 0;
					if ($this->relationships[$type][$d2]['sortable']) {
						// Incrementar el dato de ordenamiento
						$order_inv++;
						$set_order = $order_inv;
					}

					$wpdb->insert(
						$table_name,
						[
							'p2p_from'  => $item,
							'p2p_to'    => $p1,
							'p2p_type'  => $type,
							'p2p_order' => $set_order,
						],
						[
							'%d',
							'%d',
							'%s',
							'%d',
						]
					);
				}
			}

			// Si no existe, insertarlo
			if ($rowcount == 0) {
				$wpdb->insert(
					$table_name,
					[
						'p2p_from'  => $p1,
						'p2p_to'    => $item,
						'p2p_type'  => $type,
						'p2p_order' => $set_order,
					],
					[
						'%d',
						'%d',
						'%s',
						'%d',
					]
				);
			}
		}
	}

	/** Cambiar las relaciones de este tipo que inicien en p1
	 * y reemplazarlas por relacion los elementos de p2
	 */
	public function set_relation($type, $p1, $p2)
	{
		// Eliminar las anteriores relaciones
		$this->remove_relations($type, $p1);

		// Guardar las nuevas relaciones forzando el orden indicado
		$this->_add_relation($type, $p1, $p2, true);
	}

	/** Eliminar la relación de este tipo que empiece en p1 y
	 * termine en p2. Si es recíproca, borrar el otro sentido también.
	 */
	public function remove_relation($type, $p1, $p2 = false)
	{
		// Si no envía p2, entonces es la función remove_relations
		if (!$p2) {
			$this->remove_relations($type, $p1);

			return;
		}

		// Eliminar cualquier relación de este tipo desde p1
		global $wpdb;
		$table_name = $wpdb->prefix . 'p2p_posts';
		$reciprocal = $this->relationships[$type]['reciprocal'];

		$wpdb->delete(
			$table_name,
			[
				'p2p_from' => $p1,
				'p2p_to'   => $p2,
				'p2p_type' => $type,
			],
			[
				'%d',
				'%d',
				'%s',
			]
		);

		// Si es recíproca, eliminar cualquier relación de
		// este tipo hacia p1
		if ($reciprocal) {
			$wpdb->delete(
				$table_name,
				[
					'p2p_to'   => $p1,
					'p2p_from' => $p2,
					'p2p_type' => $type,
				],
				[
					'%d',
					'%d',
					'%s',
				]
			);
		}
	}

	/**
	 * Borrar todas las relaciones de este tipo que inicien en
	 * p1. En caso de ser recíproca, eliminar las que lleguen a p1
	 *
	 * @param string $type
	 * @param int $p1
	 * @return void
	 */
	public function remove_relations($type, $p1)
	{
		// Eliminar cualquier relación de este tipo desde p1
		global $wpdb;
		$table_name = $wpdb->prefix . 'p2p_posts';
		$reciprocal = $this->relationships[$type]['reciprocal'];

		$wpdb->delete(
			$table_name,
			[
				'p2p_from' => $p1,
				'p2p_type' => $type,
			],
			[
				'%d',
				'%s',
			]
		);

		// Si es recíproca, eliminar cualquier relación de
		// este tipo hacia p1
		if ($reciprocal) {
			$wpdb->delete(
				$table_name,
				[
					'p2p_to'   => $p1,
					'p2p_type' => $type,
				],
				[
					'%d',
					'%s',
				]
			);
		}
	}

	/**
	 * Retorna la lista de identificadores de estructuras
	 * relacionadas con la 'from' con la relación tipo 'type'.
	 *
	 * @param string $type
	 * @param int $p1
	 * @return array
	 */
	public function related_to($type, $p1)
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'p2p_posts';

		$sql = "SELECT p2p_from 
				FROM {$table_name} 
				WHERE p2p_to = %d 
				AND p2p_type = '%s'";

		$sql = $wpdb->prepare($sql, $p1, $type);
		$to  = $wpdb->get_col($sql);

		return $to;
	}

	/**
	 * Retorna la lista de identificadores de estructuras
	 * relacionadas con la 'from' con la relación tipo 'type'.
	 *
	 * @param string $type
	 * @param int $p1
	 * @return array
	 */
	public function related_from($type, $p1)
	{
		global $wpdb;
		$table_name = $wpdb->prefix . 'p2p_posts';

		$sql = "SELECT p2p_to 
				FROM {$table_name} 
				WHERE p2p_from = %d 
				AND p2p_type = '%s'";

		$sql = $wpdb->prepare($sql, $p1, $type);
		$to  = $wpdb->get_col($sql);

		return $to;
	}

	/**
	 * Retornar los datos de un tipo de relación
	 *
	 * @param string $type
	 * @return array
	 */
	public function relation_data($type)
	{
		if (!isset($this->relationships[$type])) {
			return false;
		}

		return $this->relationships[$type];
	}

	public function relationships($name, $id)
	{
		// Arreglo que contendrá los resultados
		$answer = [];

		$args = [
			'post_type' => 'any',
			'p2p'       => [
				'relation' => 'AND',
				[
					'type' => $name,
					'to'   => $id,
				],
			],
			'nopaging'    => true,
			'post_status' => 'any',
		];

		return get_posts($args);
	}

	public function opposite($name, $id)
	{
		$post_type = get_post_type($id);

		$relationship = $this->relationships[$name];

		if ($relationship['from']['type'] == $post_type) {
			return $relationship['to'];
		}

		if ($relationship['to']['type'] == $post_type) {
			return $relationship['from'];
		}

		return false;
	}

	public function get_post($id)
	{
		// Arreglo que contendrá los resultados
		$answer = [];

		$args = [
			'post_type'   => 'any',
			'post__in'    => [$id],
			'nopaging'    => true,
			'post_status' => 'any',
		];

		return get_posts($args);
	}

	/**
	 * Registrar el meta term en WPJson.
	 * ToDo: ¿Usar si la estructura ya exisitía y no se ha declarado el schema para estos post meta?
	 */
	public function api()
	{
		$types = [];
		foreach ($this->relationships as $relationship) {
			if (isset($relationship['name']) && isset($relationship['from']) && isset($relationship['to'])) {
				$name = 'p2p_' . $relationship['name'];
				$from = $relationship['from']['type'];
				$to   = $relationship['to']['type'];

				register_rest_field(
					$from,
					$name,
					$this->api_args($name, false)
				);

				register_rest_field(
					$to,
					$name,
					$this->api_args($name, false)
				);

				$types[] = $from;
				$types[] = $to;
			}
		}
		$types = array_unique($types);
	}

	/**
	 * Generador del arreglo a ser enviado con register_rest_field
	 * @param  string $slug        Taxonomía
	 * @param  string $description Nombre del campo
	 * @return array               El arreglo requerido para ser llamado por register_rest_field
	 */
	private function api_args($slug, $description)
	{
		return [
			'get_callback' => function ($object, $field_name, $request) {
				$a = get_post_meta($object['id'], $field_name, true);

				return $a;
			},
			'update_callback' => function ($value, $object, $field_name) {
				$a = update_post_meta($object->ID, $field_name, wp_slash($value));

				return $a;
			},
			'show_in_rest' => true,
			'schema'       => [$slug => $description],
		];
	}

	/**
	 * Atender almacenamiento del postmeta y crear las relaciones.
	 * 
	 * Esto se hace para los casos en que el almacenamiento se hace
	 * directamente por el postmeta y se necesite asegurar la creación
	 * de las relaciones en la tabla p2p.
	 *
	 * @param null|bool $check
	 * @param int $object_id
	 * @param string $meta_key
	 * @param mixed $meta_value
	 * @param bool $unique
	 * @return null|bool
	 */
	public function save_meta($check, $object_id, $meta_key, $meta_value, $unique)
	{
		if (substr($meta_key, 0, 4) === 'p2p_') {
			// Nombre de la relación
			$relationship = substr($meta_key, 4);

			// Retirar corchetes
			$list = str_replace(['[', ']'], '', $meta_value);
			$list = array_map('intval', explode(',', $list));
			$a    = 1;
			$this->set_relation($relationship, $object_id, $list);
		}

		return $check;
	}

	/**
	 * Obtener el valor real del meta de la relación.
	 *
	 * Es posible que el valor del meta esté desactualizado así que se consulta
	 * el valor real a partir de la tabla de relaciones.
	 *
	 * @param mixed $metadata
	 * @param int $object_id
	 * @param string $meta_key
	 * @param bool $single
	 * @return mixed
	 */
	public function get_meta($metadata, $object_id, $meta_key, $single)
	{
		// Si el meta inicia con 'p2p_'
		if (substr($meta_key, 0, 4) === 'p2p_') {
			// Obtenr la lista de posts relacionadas
			$list = $this->relationships(str_replace('p2p_', '', $meta_key), $object_id);

			// Extraer solo la lista de ids
			$list = array_map(function ($v) { return $v->ID; }, $list);

			// Concatenar la lista
			$metadata = sprintf('[%s]', implode(',', $list));
		}

		// Retornar el metadato
		return $metadata;
	}

	/**
	 * Guardar los datos de este PostMeta
	 * @param  int $post_id ID de la estructura que se está almacenando
	 */
	public function save($post_id)
	{
		global $pla_save_flag, $post;

		// Verificar que no sea una rutina de autoguardado.
		// Si es así, el formulario no ha sido enviado y por lo tanto cancelamos
		// el proceso.
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return;
		}

		// Revisar permisos del usuario
		if (!current_user_can('edit_post', $post_id)) {
			return;
		}

		// Almacenar variables

		// Si estamos editando desde la vista de edición
		// Como estamos usando javascript / rest, esta parte ya no se usa, pero se
		// deja acá para referencia
		if (isset($_POST['action']) && $_POST['action'] == 'editpost') {
			// Recorrer todos los nombres d elas relacione sy ver si hay alguna en POST
			foreach ($this->relationships as $relationship) {
				$key = 'p2p_' . $relationship['name'];
				if (isset($_POST[$key])) {
					$value = apply_filters('p2p/before_update_postmeta', $_POST[$key], $key, $post_id);
					update_post_meta($post_id, $key, wp_slash($value));
				}
			}
		}
	}

	/**
	 * Obtener lista filtrada de relaciones para los dos tipos consultados
	 *
	 * @param string $type_1
	 * @param string $type_2
	 * @return array
	 */
	public function get_relationships_by_types($type_1, $type_2)
	{
		return array_filter($this->relationships, function ($item, $key) use ($type_1, $type_2) {
			return (
				($item['from']['type'] == $type_1 && $item['to']['type'] == $type_2) ||
				($item['from']['type'] == $type_2 && $item['to']['type'] == $type_1)
			);

		}, ARRAY_FILTER_USE_BOTH);
	}
}
