<?php

namespace Baxtian;

/**
 * Administrar los plugins requeridos
 */
class WP_P2P_UI
{
	private $relationship;
	private $post_type;
	private $label;
	private $cardinality;
	private $sortable;
	private $allow_create;
	private $direction;
	private $lang_domain;
	private $lang_path;
	private $ui;
	private $icon;

	/**
	 * Declarar acciones y filtros para WP_P2P_UI
	 *
	 * @return void
	 */
	public function __construct($relationship, $direction, $data)
	{
		// El tipo y la crdinalidad están del otro lado del relacionamiento
		$this->post_type = ($direction == 'from') ? $data['to']['type'] : $data['from']['type'];
		$this->ui        = $data[$direction]['ui'];
		$this->icon      = $data[$direction]['icon'];
		//$this->cardinality = ($direction == 'from') ? $data['to']['cardinality'] : $data['from']['cardinality'];

		// Los datos son los de la dirección solicitada
		$data = $data[$direction];

		$this->relationship = $relationship;
		$this->cardinality  = $data['cardinality'];
		$this->label        = $data['title'];
		$this->sortable     = $data['sortable'];
		$this->direction    = $direction;
		$this->allow_create = $data['allow_create'];
		$this->lang_domain  = 'wp_p2p';

		$locale = apply_filters('plugin_locale', determine_locale(), $this->lang_domain);

		$wp_component    = basename(WP_CONTENT_URL);
		$_path           = explode($wp_component, __DIR__);
		$this->lang_path = dirname(WP_CONTENT_DIR . $_path[1]) . '/languages/';
		$mofile          = $this->lang_path . $this->lang_domain . '-' . $locale . '.mo';

		load_textdomain($this->lang_domain, $mofile);

		add_filter('load_script_textdomain_relative_path', [$this, 'textdomain_relative_path'], 10, 2);
		add_action('add_meta_boxes', [$this, 'register_box']);
	}

	/**
	 * Corrige la URL relatica del script para ser empleado con el traductor de idioma.
	 * @param  string $relative URL relativa al script con los textos a ser traducidos
	 * @param  string $src      URL completa del script
	 * @return string           URL relativa corregida
	 */
	public function textdomain_relative_path($relative, $src)
	{
		// Hacerle creer a WP que el relativo empieza en la raiz de esta
		// librería y no en el vendor de quien llama a esta librería.
		$relative = str_replace('vendor/baxtian/wp_p2p/', '', $relative);

		return $relative;
	}

	public function register_box()
	{
		$current_screen = get_current_screen();
		$is_gutenberg   = method_exists($current_screen, 'is_block_editor') && $current_screen->is_block_editor();

		// Solo cargar si esta página es del tipo
		if ($current_screen->post_type == $this->post_type) {
			if ($is_gutenberg) {
				add_action('admin_enqueue_scripts', [$this, 'gutenberg']);
			} else {
				add_meta_box(
					$this->relationship,
					$this->label,
					[$this, 'metabox'],
					[$this->post_type],
					'side',
				);
			}
		}
	}

	public function metabox()
	{
		global $post;

		// Solo activar si estamos en un post_type para esta caja
		if ($post->post_type == $this->post_type) {
			do_action('p2p/metabox', $post->ID, $this->relationship);
		}
	}

	/**
	 * Declarar scripts requeridos para un PostMeta tipo block
	 * @param  string $hook Gancho
	 */
	public function gutenberg($hook)
	{
		// Solo activar si estamos en un post_type para esta caja
		$screen = get_current_screen();
		if ($screen->post_type != $this->post_type) {
			return;
		}

		// Directorio de assets
		$wp_component = basename(WP_CONTENT_URL);
		$path         = dirname(__DIR__);
		$_path        = explode($wp_component, $path);
		$url          = content_url(str_replace(DIRECTORY_SEPARATOR, '/', $_path[1])) ;

		// Archivo de dependencias
		$asset_file = include($path . '/build/script-side.asset.php');

		// Registrar p2p-side
		wp_register_style('p2p_side', $url . '/build/style-side.css', [], $asset_file['version']);
		// 'wp-plugins', 'wp-edit-site', 'wp-element'
		wp_register_script('p2p_side', $url . '/build/script-side.js', $asset_file['dependencies'], $asset_file['version']);
		wp_set_script_translations('p2p_side', $this->lang_domain, $this->lang_path);

		// Registrar script box
		wp_enqueue_style('p2p_side');
		wp_enqueue_script('p2p_side');

		// Argumentos
		$args = [
			'name'         => str_replace('_', '-', $this->relationship . '_' . $this->direction),
			'relationship' => $this->relationship,
			'title'        => $this->label,
			'cardinality'  => $this->cardinality,
			'sortable'     => $this->sortable,
			'ui'           => $this->ui,
			'icon'         => $this->icon,
			'domain'       => $this->lang_domain,
			'allow_create' => $this->allow_create,
		];

		// Las variables a ser enviadas para este bloque
		wp_add_inline_script('p2p_side', "
			try{
				p2pSide('" . json_encode($args) . "');
			}catch(e){}");
	}
}
