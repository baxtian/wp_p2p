const glob = require('glob');
const path = require('path');
const webpack = require("webpack");
const CopyPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require("terser-webpack-plugin");
const RemoveEmptyScriptsPlugin = require('webpack-remove-empty-scripts');

// Determinar si es entorno de producción
const isProduction = process.env.NODE_ENV === '';

/***** Listado de entradas a ser compiladas / tratadas *******/ 

// Estilos y scripts base: se almacenarán en assets/(css|scripts) 
var main = glob.sync('./assets/@(*.js|*.scss|*.css)').reduce((acc, path) => {
			const entry = path.replace('./assets/', '').replace(/\.js|\.css|\.scss/, '');
			acc[entry] = path
			return acc
		},
		{});

// Indicar que usaremos en la variable de entorno de entradas estas tres listas.
process.env.WP_ENTRY = JSON.stringify({ ...main });

// Configuración inicial de webpack para Wordpress
const defaultConfig = require('./node_modules/@wordpress/scripts/config/webpack.config.js');

// Regla para compilar CSS
const cssLoaders = [
	{
		loader: MiniCssExtractPlugin.loader
	}, {
		loader: 'css-loader',
		options: {
			sourceMap: !isProduction,
			importLoaders: 1
		}
	}, {
		loader: 'resolve-url-loader',
		options: {}
	}
];

// Recorrer las reglas y alterar los usos
defaultConfig.module.rules.forEach((rule, index) => {
	// Si es css, usar cssLoader
	if (String(rule.test).indexOf('css') !== -1) {
		defaultConfig.module.rules[index].use = [
			...cssLoaders,
		];
	}

	// Si es scss, usar cssLoader y agregar al final sass-loader
	if (String(rule.test).indexOf('(sc|sa)ss') !== -1) {
		defaultConfig.module.rules[index] = {
			test: /\.(sc|sa|c)ss$/,
			exclude: /node_modules/,
			use: [
				...cssLoaders, {
					loader: 'sass-loader',
					options: {
						implementation: require("sass"),
						sourceMap: !isProduction
					}
				}
			]
		};
	}

	// Si es imagen, modificar el directorio para que quede en /build/media/images
	if (String(rule.test).indexOf('png') !== -1) {
		rule.generator.filename = 'media/images/[name].[hash:8][ext]';
		defaultConfig.module.rules[index] = rule;
	}

	// Si es fuente, modificar el directorio para que quede en /build/media/fonts
	if (String(rule.test).indexOf('woff2') !== -1) {
		rule.generator.filename = 'media/fonts/[name].[hash:8][ext]';
		defaultConfig.module.rules[index] = rule;
	}
});

// Reglas a ser exportadas para modificar el webpack
module.exports = {
	...defaultConfig,
	output: {
		// Almacenar los elementos compilados en build
		path: path.resolve(process.cwd(), 'build'),
	},
	// externals: {
	// 	...defaultConfig.externals,
	// 	// jquery: 'jQuery',
	// },
	optimization: {
		minimizer: [
			// Minificar los scripts
			new TerserPlugin(),
			// Minificar los estilos
			new CssMinimizerPlugin()
		],
	},
	plugins: [
		...defaultConfig.plugins,
		// Retirar los js de los compilados de estilos
		new RemoveEmptyScriptsPlugin(),
		// Reglas para minificar los estilos
		new MiniCssExtractPlugin(
			{
				filename: '[name].css',
				chunkFilename: '[id].css'
			}
		),
		// new webpack.ProvidePlugin({ $: "jquery", jQuery: "jquery", }),
		// Reglas para copiar estilos, scrpts e imágenes que no se compilan
		// pero que si hacen parte de las librerías
		new CopyPlugin({
			patterns: [
				{
					// Copiar en build/media/libs todo desde la librería menos los scss y los json de cada librería
					from: "assets/libs",
					to: "libs",
					noErrorOnMissing: true,
					filter: async (resourcePath) => {
						if (resourcePath.endsWith('/libs/lib.json')) {
							return true;
						}
						const file_extension = resourcePath.split('.').pop();
						if (file_extension == "json" || file_extension == "scss") {
							return false;
						}

						return true;
					},
				},
				{
					// Copiar en build/media/images todo desde el directoriod e imágenes en assets
					from: "assets/img",
					to: "media/images",
					noErrorOnMissing: true,
				}
			],
		}),
	]
};
