/**
* WordPress dependencies.
*/
import { registerPlugin } from '@wordpress/plugins';

/**
 * Local dependencies.
 */
import P2PSidebar from './components/P2PSidebar.js';

const { Component } = wp.element;
const { getCurrentPostId, getCurrentPostType } = wp.data.select("core/editor");
const post_id = getCurrentPostId();
const post_type = getCurrentPostType();

/**
 * Register the MetaTags plugin.
 */
global.p2pSide = function (json) {
	var data = JSON.parse(json);
	registerPlugin("wp-p2p-" + data.name, {
		icon: false,
		render: function (props) {
			return <P2PSidebar
				title={data.title}
				name={data.name}
				relationship={data.relationship}
				cardinality={data.cardinality}
				sortable={data.sortable}
				allow_create={data.allow_create}
				post_id={post_id}
				post_type={post_type}
				ui={data.ui}
				icon={data.icon}
			/>;
		}
	});
}
