jQuery(document).ready(function ($) {
	const p2p_insert_item = function ($list, item, cardinality) {
		const title_class = (cardinality == 'ONE') ? " no_handler" : "";
		const handler = (cardinality == 'ONE') ? `<div></div>` : `<div class="handler ui-sortable-handle">
			<span class="disable-select dragHandle dashicons dashicons-menu-alt2"></span>
		</div>`;
		const txt = `<div class="item" data-id="${item.id}">
			${handler}
			<div class="title${title_class}">${item.name}</div>
			<div class="actions">
				<span class="dashicons dashicons-trash delete" data-id="${item.id}"></span>
			</div>
		</div>`;

		$($list).find(".item").each(function (index, node) {
			if ($(node).attr('data-id') == item.id) {
				$(node).remove();
			}
		});

		if (cardinality == 'ONE')
			$($list).html(txt);
		else
			$($list).append(txt);

		p2p_actualizarMeta($list);
	}

	const p2p_create_opposite_entry = function ($list, item, cardinality, relationship, post_id) {
		var url = p2p_metabox.wp_json + "p2p/v1/create_opposite_entry/" + relationship + "/" + post_id + "?name=" + item.name;
		jQuery.ajax({
			url: url,
			type: 'POST',
			success: function (res) {
				if(res && res.length > 0) {
					item.id = res[0].ID;
					p2p_insert_item($list, item, cardinality)
				}
			}
		})
	}

	// Actualizar lista
	const p2p_actualizarMeta = function (el) {
		const widget = $(el).sortable("widget");
		const children = $(widget).find(".item");
		let order = [];
		children.each((index, item) => { order.push($(item).attr('data-id')); });
		$(el).next().val("[" + order.join(",") + "]");

		if (children.length > 0) {
			$(el).parent().addClass('has_items');
		} else {
			$(el).parent().removeClass('has_items');
		}
	}

	const p2p_options = function (element, cardinality) {
		const self = this;
		const $input = $(element);
		const $parent = $input.parent();
		const $list = $parent.next();
		var post_id = $input.attr('data-id');
		var relationship = $input.attr('data-relationship');
		var allow_create = ($input.attr('data-allow-create') == '1') ? true : false;
		var lbl_create = $input.attr('data-create-text');
		if (post_id === undefined || relationship === undefined)
			return;

		var options = {
			getValue: "name",
			highlightPhrase: false,
			template: {
				type: "custom",
				method: function (value, item) {
					if (item.id == "-1")
						return "<span title='" + lbl_create + "' class='dashicons dashicons-admin-post'></span><span class='create'>" + $input.val() + "</span>";
					return value;
				}
			},
			list: {
				maxNumberOfElements: 10000,
				onClickEvent: function () { // Click en elemento de la lista
					//Asignar al campo de dato el valor seleciconado
					var item = $input.getSelectedItemData();
					if (item.id == -1)
						p2p_create_opposite_entry($list, item, cardinality, relationship, post_id);
					else
						p2p_insert_item($list, item, cardinality);
					$input.val('');
				},
				onChooseEvent: function () { // Click en elemento de la lista
					//Asignar al campo de dato el valor seleciconado
					var item = $input.getSelectedItemData();
					if (item.id != -1)
						p2p_insert_item($list, item, cardinality);
					$input.val('');
				},
				onLoadEvent: function () { //Se carga nueva lista
					//Indicar que ya no estamos buscando
					$parent.removeClass('searching');
					//Pasar a fondo de color blanco
					$input.stop().animate({
						'background-color': 'white'
					}, 400);
					// Si se permite crear
					if (allow_create) {
						// Si no hay elementos en la lista, 
						// agregar la opción para crear
						if ($input.next().find('li').length == 0) {
							$input.find('.easy-autocomplete-container ul').append("<li>Show all</li>");
						}
					}
				},
			}
		};

		options['listLocation'] = "items";
		options['matchResponseProperty'] = "query";
		options['requestDelay'] = 500;
		options['url'] = function (phrase) {
			var not_in = $(element).parent().parent().parent().find('.p2p_meta_value').val();
			var url = p2p_metabox.wp_json + "p2p/v1/options?eac=1&name=" + relationship + "&id=" + post_id + "&text=" + phrase + "&not__in=" + not_in;
			return url;
		};

		$input.easyAutocomplete(options);

		$input.on('blur', function () {
			setTimeout(function () {
				$input.val('');
			}, 400);
		});

		$input.on('keyup', function (e) {
			//SI es enter o tab, usar el primero de la lista
			if (e.which > 40 || e.which === 8) {
				exact_match = false;
				//Si es un nuevo caracter indicar la carga si hay texto en el input
				if ($input.val().length > 0) {
					if (!$parent.hasClass('searching')) {
						$parent.addClass('searching');
						$input.stop().animate({
							'background-color': '#fff2f2'
						}, 400);
					}
				} else {
					$parent.removeClass('searching');
					$input.stop().animate({
						'background-color': 'white'
					}, 400);
				}
			}
		});
	}

	$(".wp_p2p_metabox").each(function (index, item) {
		const cardinality = $(item).data("cardinality");
		const list = $(item).find('.list');

		// Inicializar selector
		p2p_options($(item).find(".p2p_options .search_field"), cardinality);

		// Ordenamiento
		$(list).sortable({
			handle: ".handler",
			update: function (event, ui) {
				p2p_actualizarMeta(list);
			}
		});

		// Eliminar
		$(list).on("click", ".item .actions .delete", function () {
			$(this).parent().parent().remove();
			p2p_actualizarMeta(list);
		});

	});
});
