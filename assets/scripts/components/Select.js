/**
 * WordPress dependencies
 */
const { __ } = wp.i18n;
import React from 'react';
import Select2 from 'react-select';

const Select = (props) => {
	return <>
		<Select2
			className={props.className}
			classNamePrefix="wp_p2p_selector"
			value={props.option || ''}
			options={props.options}
			onChange={props.onSelectChange}
			onInputChange={props.onInputChange}
			loadingMessage={() => __('Searching...', 'wp_p2p')}
			noOptionsMessage={() => __('No options', 'wp_p2p')}
			placeholder={__('Search', 'wp_p2p')}
			styles={{
				option: (baseStyles, { data, isDisabled, isFocused, isSelected }) => ({
					...baseStyles,
					color: data.value == -1 ? "red" : "inherit",
					':before': {
						...baseStyles[':after'],
						fontFamily: "dashicons",
						content: '""',
						color: "black",
						display: data.value == -1 ? "inline-block" : "none",
						lineHeight: "18px",
						fontWeight: 400,
						fontStyle: "normal",
						fontSize: "20px",
						paddingRight: "5px",
						verticalAlign: "middle",
					},
				}),
			}}
		/>
	</>;
}

export default Select;
