/**
 * WordPress dependencies.
 */
import { __ } from '@wordpress/i18n';
import { compose } from "@wordpress/compose";
import { withSelect, withDispatch } from '@wordpress/data';
import { TextControl, PanelBody } from '@wordpress/components';
import { PluginDocumentSettingPanel, PluginSidebar } from '@wordpress/edit-post';
import apiFetch from '@wordpress/api-fetch';
import { useState, useEffect, useRef } from '@wordpress/element';
import SortableList, { SortableItem, SortableKnob } from 'react-easy-sort'
import arrayMove from 'array-move'

import Select from './Select.js';

/**
 * Sidebar component for the gutenberg editor.
 */
const P2PSidebar = (props) => {

	let {
		relationship,
		title,
		cardinality,
		sortable,
		metaValue,
		setMetaValue,
		ui,
		icon
	} = props;

	const containerRef = useRef();
	const post_id = wp.data.select('core/editor').getCurrentPostId();

	const [posts, setPosts] = useState([]);
	const [option, setOption] = useState('');
	const [options, setOptions] = useState([]);
	const [loading, setLoading] = useState(false);

	const debounce = function (fn, delay = 250) {
		let timeout;

		return (...args) => {
			clearTimeout(timeout);
			timeout = setTimeout(() => {
				fn(...args);
			}, delay);
		};
	}

	// Retirar item de la lista
	const onRemove = (id) => {
		const newPosts = posts.filter((value) => {
			if (value.ID == id) {
				return false;
			}
			return true;
		});
		onSetMeta(newPosts);
	};

	// Seleccionar opción en el selector
	const onSelectOption = (newOption) => {
		setLoading(true);

		// Si el ID es -1 entonces hay que crearla primero.
		if (newOption.value == -1) {
			wp.apiFetch({
				path: "/p2p/v1/create_opposite_entry/" + relationship + "/" + post_id + "?name=" + newOption.label,
				method: 'POST'
			}).then(res => {
				// Asignar las opciones
				setLoading(false);
				if (res && res.length > 0) {
					const post = res.shift();
					const newList = posts.filter((item) => {
						return (item.ID != post.ID) ? true : false;
					});
					const newPosts = [...newList, post];
					onSetMeta(newPosts);

					// Asignar la nueva opción
					setOptions([{
						value: post.ID,
						label: post.post_title
					}]);
				}
			});
		} else {

			wp.apiFetch({
				path: "/p2p/v1/post?id=" + newOption.value
			}).then(res => {
				// Asignar las opciones
				setLoading(false);
				if (res.length > 0) {
					const post = res.shift();
					const newList = posts.filter((item) => {
						return (item.ID != post.ID) ? true : false;
					});
					const newPosts = [...newList, post];
					onSetMeta(newPosts);
				}
			});
		}
	}

	// Buscar opciones a partir d ela búsqueda
	const onSearchOptions = (search, data) => {
		setOption(search);
		//Only search for new options if this is an input-change
		if (data.action == 'input-change') {
			wp.apiFetch({
				path: "/p2p/v1/options?name=" + relationship + "&id=" + post_id + "&text=" + search + "&not__in=" + metaValue + "&eac=1"
			}).then(posts => {
				var answer = posts.items.map((item) => {
					return {
						value: item.id,
						label: item.name
					}
				})

				setOptions(answer);
			});
		}
	}

	// Cambiar el orden
	const onSetOrder = function (oldIndex, newIndex) {
		const _items = arrayMove(posts, oldIndex, newIndex);
		onSetMeta(_items);
	}

	// Cambios en posts y meta
	const onSetMeta = function (newList) {
		const text_array = "[" + newList.map((item) => item.ID).join(",") + "]";
		setMetaValue(text_array);

		setPosts(newList);
	}

	// Función inicial
	const onInit = async () => {
		// Posts iniciales
		const path = '/p2p/v1/relationships?name=' + relationship + '&id=' + post_id;
		const res = await apiFetch({
			path: path,
			method: 'GET',
		});
		onSetMeta(res);

		return;
	};

	// Effects
	useEffect(() => { onInit(); }, []);

	// Selector
	let selector = "";
	if (
		cardinality == 'ANY' ||
		cardinality == 'MANY' ||
		(cardinality == 'ONE' && posts.length == 0)
	) {
		selector = <Select
			className='wp_p2p_select'
			onSelectChange={onSelectOption}
			onInputChange={debounce(onSearchOptions, 500)}
			option={option}
			options={options}
		/>;
	} else {

	}

	const with_handler = (cardinality == 'MANY' && sortable);

	const render_list = (with_handler) ?
		<SortableList onSortEnd={onSetOrder} draggedItemClassName="moviendo">
			{posts.map((item, index) => (
				<SortableItem key={item}>
					<div className="wp_p2p_sortable_item item">
						<div class="item">
							<SortableKnob>
								<div className="handler">
									<span className="disable-select dragHandle dashicons dashicons-menu-alt2"></span>
								</div>
							</SortableKnob>
							<div className={"title" + (!with_handler ? " no_handler" : "")}>{item.post_title}</div>
							<div className="actions">
								<span className="dashicons dashicons-trash" onClick={() => {
									onRemove(item.ID);
								}}></span>
							</div>
						</div>
					</div>
				</SortableItem>
			))}
		</SortableList> :
		<>{posts.map((item, index) => (
			<div className="item">
				<div className={"title" + (!with_handler ? " no_handler" : "")}>{item.post_title}</div>
				<div className="actions">
					<span className="dashicons dashicons-trash" onClick={() => {
						onRemove(item.ID);
					}}></span>
				</div>
			</div>
		))}</>;

	const content = <>
		{selector}
		<div
			className={"list" + (loading ? " loading" : "")}
			ref={containerRef}
			style={{ touchAction: "pan-y" }}
		>
			{render_list}
		</div>
		<TextControl
			value={metaValue}
			onChange={(newValue) => setMetaValue(newValue)}
			type="hidden"
		/>
	</>;

	return (ui == 'auto') ? (
		<PluginDocumentSettingPanel
			name={relationship}
			title={title}
			className="wp_p2p_side"
			icon={icon}
		>
			{content}
		</PluginDocumentSettingPanel >
	) : (
		<PluginSidebar
			name={relationship}
			title={title}
			className="wp_p2p_side"
			icon={icon}
		>
			<PanelBody>
				{content}
			</PanelBody>
		</PluginSidebar >
	);
};

export default compose(withDispatch(function (dispatch, props) {
	const metaKey = "p2p_" + props.relationship;
	return {
		setMetaValue: function (metaValue) {
			dispatch('core/editor').editPost({
				[metaKey]: metaValue
			});
		}
	}
}), withSelect(function (select, props) {
	const metaKey = "p2p_" + props.relationship;
	return {
		metaValue: select('core/editor').getEditedPostAttribute(metaKey)
	}
}))(P2PSidebar);
